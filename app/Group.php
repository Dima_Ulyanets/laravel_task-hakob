<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{

    public function users() {
        return $this->belongsToMany('App\User', 'user_group_map', 'group_id', 'user_id')->withPivot('role_id');
    }

    public function type() {
    	return $this->belongsTo('App\GroupType', 'group_type_id', 'id');
    }


    public function isPublic() {
    	return $this->type->title == 'public';
    }

}
