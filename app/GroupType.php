<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupType extends Model
{
    protected $fillable = ['title'];

    public function groups() {
    	return $this->hasMany('App\Group');
    }

    public static function getPublic() {
    	return self::where('title', '=', 'public')->first();
    }

}
