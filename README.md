# Task

Acceptance criteria:

* It should be REST API
* No UI needed. Purely REST API.
* There should be table `groups`. It should have id, title and description fields.
* Group could be `private` or `public`
* There should be `users` table
* User can be `admin`, `member` or have no relation at all with group
* There should be 2 endpoints: GET specific group with admins and members in separate arrays and PUT specific group
* User should have access to GET specific group in case it's public or if he is admin/member of that group
* User should have access to PUT specific group in case if he is admin of that group
* No need to create endpoints for user, just make few seeds

What matters:

* Code style/readability
* Architecture
* Proper error handling
* Proper database handling

This task is unpaid unless you will be selected for current job. So, try to spend your time accordingly, and if you think you out of time but you have in mind some improvements in your current implementation, it's absolutely fine just to leave comments what you would do. Just leave detailed comments in this case.