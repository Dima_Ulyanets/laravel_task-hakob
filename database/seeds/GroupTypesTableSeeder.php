<?php

use Illuminate\Database\Seeder;
use App\GroupType;

class GroupTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        GroupType::truncate();
        GroupType::create(['title' => 'public']);
        GroupType::create(['title' => 'private']);
    }
}
