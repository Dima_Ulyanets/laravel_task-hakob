<?php

use Illuminate\Database\Seeder;


class UserGroupMapSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_group_map')->truncate();
        $users = App\User::all();
        $roles = App\Role::all();
        $groups = App\Group::all();

        $map = [];
        foreach ($users as $user) {
        	$map[] = [
        		'user_id' => $user->id,
        		'group_id' => $groups->random()->id,
        		'role_id' => $roles->random()->id
        	];
        }

        DB::table('user_group_map')->insert($map);
    }
}
